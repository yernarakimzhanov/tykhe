import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:provider/provider.dart';
import 'package:tykhe/screens/profile/rates.dart';
import 'package:tykhe/screens/profile/savings_history.dart' as SavingsHistory;
import 'package:tykhe/state/SavingsAccount.dart';
import 'package:tykhe/modals/settings.dart';


import '../modals/additional_contribution_modal.dart';
import '../modals/reminder.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:intl/intl.dart';

final formatCurrency = new NumberFormat.compactSimpleCurrency(decimalDigits: 2);

class TopAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 4, right: 4),
      child: AppBar(
        leading: IconButton(
            icon: SvgPicture.asset('assets/svg/reminder_icon.svg'),
            onPressed: () => {
                  showMaterialModalBottomSheet(
                      expand: true,
                      isDismissible: true,
                      context: context,
                      builder: (context) => Reminder())
                }),
        actions: [
          IconButton(
            icon: SvgPicture.asset('assets/svg/settings_icon.svg'),
            onPressed: () => {
              showMaterialModalBottomSheet(
                  expand: true,
                  isDismissible: true,
                  context: context,
                  builder: (context) => ProfileOptions())
            },
          ),
        ],
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        bottomOpacity: 1,
      ),
    );
  }
}

class CurrentSavingsCard extends StatefulWidget {
  final int savingsAmount;

  CurrentSavingsCard(this.savingsAmount);

  @override
  _CurrentSavingsState createState() => _CurrentSavingsState();
}

class _CurrentSavingsState extends State<CurrentSavingsCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 10.0),
        height: 116,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(0)),
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 15, top: 20, bottom: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Current Savings',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 14,
                          ),
                        ),
                        Spacer(),
                        Text('${formatCurrency.format(widget.savingsAmount)}',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              letterSpacing: 2,
                              color: Colors.white,
                              fontSize: 36,
                            ))
                      ],
                    )),
                Spacer(),
                FlatButton(
                  child: SvgPicture.asset('assets/svg/plus.svg'),
                  onPressed: () {
                    showMaterialModalBottomSheet<void>(
                        isDismissible: true,
                        context: context,
                        builder: (context) => AdditionalContributionModal());
                  },
                ),
              ],
            )));
  }
}

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: Consumer<SavingsAccount>(
          builder: (context, savingsaccount, child) {
            return Scaffold(
              body: Builder(
                builder: (context) => Scaffold(
                  appBar: TopAppBar(),
                  body: new SingleChildScrollView(
                      child: Container(
                          margin: const EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Text(
                                  "Welcome, ${savingsaccount.user.email}",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    height: 1.2,
                                  ),
                                ),
                              ),
                              CurrentSavingsCard(
                                  savingsaccount.savingsAccount.savingsAmount),
                              Rates(
                                period: savingsaccount.savingsAccount.savingsPeriod,
                                savingsPerPeriod:
                                    savingsaccount.savingsAccount.savingsAmount,
                                estimatedSavings:
                                    savingsaccount.savingsAccount.savingsAmount,
                                missingAmount:
                                    savingsaccount.savingsAccount.savingsGoal,
                                byAge: savingsaccount
                                    .savingsAccount.savingsStopAge,
                              ),
                              SavingsHistory.SavingsHistory()
                            ],
                          ))),
                ),
              ),
            );
          },
        ),
        onWillPop: () async => false);
  }
}
