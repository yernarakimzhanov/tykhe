import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tykhe/custom_components/base_container.dart';
import 'package:tykhe/custom_components/custom_top_bar.dart';
import 'package:tykhe/custom_components/sign_in.dart';
import 'package:tykhe/forms/set_savings_form.dart';

class SetSavings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: CustomTopBar(
              "Set your savings",
              removeLeftIcon: false,
              disconnectAuth: true,
            ),
            body: BaseContainer(SetSavingsForm())));
  }
}
