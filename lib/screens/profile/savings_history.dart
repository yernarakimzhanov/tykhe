import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tykhe/state/SavingsAccount.dart';

class SavingsHistory extends StatefulWidget {
  @override
  _SavingsHistoryState createState() => _SavingsHistoryState();
}

class _SavingsHistoryState extends State<SavingsHistory> {
  var historyList = [];
  final snackBar =
      SnackBar(content: Text('Sorry! Couldn`t connect to server.'));

  @override
  void initState() {
    super.initState();
    fetchList();
  }

  List<Widget> _buildSavingsHistoryCards() {
    List<Widget> listButtons = List<Widget>.generate(historyList.length, (i) {
      return Card(
        child: ListTile(
          title: Text(historyList[i].date),
          subtitle: Text('Savings amount: ${historyList[i].amount}'),
          trailing: OutlinedButton(
            onPressed: () {
              print('pressed');
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
            ),
            child: Text(
              '${historyList[i].status.toString().toUpperCase()}',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      );
    });
    return listButtons;
  }

  void fetchList() async {
    try {
      historyList = await Provider.of<SavingsAccount>(context, listen: false)
          .fetchSavingsHistory();
      print(historyList);
    } catch (e) {
      print(e);
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Savings history',
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
          Text(
            'Weekly',
            style: TextStyle(fontWeight: FontWeight.w600),
          )
        ],
      ),
      ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: _buildSavingsHistoryCards(),
      ),
    ]);
  }
}
