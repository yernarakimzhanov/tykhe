import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Rates extends StatefulWidget {
  final String period;
  final int savingsPerPeriod;
  final int missingAmount;
  final int byAge;
  final int estimatedSavings;

  Rates({
    @required this.period,
    @required this.savingsPerPeriod,
    @required this.missingAmount,
    @required this.byAge,
    @required this.estimatedSavings,
  });

  @override
  _RatesState createState() => _RatesState();
}

class _RatesState extends State<Rates> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(0)),
      child: Table(
        children: [
          TableRow(children: [
            ListTile(
              title: Text(
                '${widget.period} contributions',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                ),
              ),
              subtitle: Text(widget.savingsPerPeriod.toString(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                  )),
            ),
            ListTile(
              title: Text(
                'Missing',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                ),
              ),
              subtitle: Text(widget.missingAmount.toString(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                  )),
            ),
          ]),
          TableRow(children: [
            // There are two dividers below because TableRow forces array to have equal amoubnt of elements
            Divider(
              thickness: 2,
            ),
            Divider(
              thickness: 2,
            )
          ]),
          TableRow(children: [
            ListTile(
              title: Text(
                'By age',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                ),
              ),
              subtitle: Text(widget.byAge.toString(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                  )),
            ),
            ListTile(
              title: Text('Estimated savings',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  )),
              subtitle: Text(widget.estimatedSavings.toString(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                  )),
            ),
          ])
        ],
      ),
    );
  }
}
