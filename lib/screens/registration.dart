import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tykhe/custom_components/sign_in.dart';
import 'package:url_launcher/url_launcher.dart';

class Registration extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Container(
            margin: const EdgeInsets.only(
                left: 10, right: 10, top: 100, bottom: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Lottie.asset(
                  'assets/animations/tykhe_logo.json',
                  height: 300,
                  animate: true,
                  repeat: false,
                ),
                Spacer(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    child: FlatButton(
                      textColor: Colors.white,
                      disabledColor: Colors.grey,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      onPressed: () async {
                        //TODO: Change from test link to real link
                        const url =
                            'https://www.admissionstesting.org/footer/terms-and-conditions/';
                        if (await canLaunch(url)) {
                          await launch(url);
                        }
                      },
                      child: Text.rich(
                        TextSpan(
                          style: TextStyle(height: 0.875, fontSize: 14),
                          text: 'Check out our ',
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Terms of Service.',
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    height: 0.875,
                                    fontSize: 14)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                    width: double.infinity,
                    height: 40,
                    padding: EdgeInsets.only(left: 24.0, right: 24.0),
                    child: SignIn()),
              ],
            )));
  }
}
