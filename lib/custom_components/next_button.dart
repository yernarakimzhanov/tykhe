import 'package:flutter/material.dart';

class NextButton extends StatelessWidget {
  final String customText;
  final whereNext;
  final GlobalKey<FormState> formKey;

  NextButton(this.customText, this.whereNext, this.formKey);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 56,
      width: double.infinity,
      child: RaisedButton(
        shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(0)),
        color: Colors.black,
        child: Text(
          "$customText".toUpperCase(),
          style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.w600,
              letterSpacing: 2.5),
        ),
        onPressed: () {
          if (formKey.currentState.validate()) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => whereNext));
          }
        },
      ),
    );
  }
}
