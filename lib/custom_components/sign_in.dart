import 'dart:async';
import 'dart:io' show Platform;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:provider/provider.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter/material.dart';
import 'package:tykhe/screens/profile.dart';
import 'package:tykhe/screens/set_savings.dart';
import 'package:tykhe/state/SavingsAccount.dart';

GoogleSignIn googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
  ],
);

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  GoogleSignInAccount _currentUser;
  final snackBar =
      SnackBar(content: Text('Sorry! Couldn`t connect to server.'));

  Future<bool> _checkIfSavingsAccountExists(userId) async {
    final account = await Provider.of<SavingsAccount>(context, listen: false)
        .fetchSavingsAccount(userId.toString());
    return account is SavingsAccountModel ? true : false;
  }

  @override
  void initState() {
    super.initState();
    googleSignIn.onCurrentUserChanged
        .listen((GoogleSignInAccount account) async {
      setState(() {
        _currentUser = account;
      });
      googleSignIn.signInSilently();
      if (_currentUser != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
            await _currentUser.authentication;
        final User user =
            await fetchUser(googleSignInAuthentication.accessToken);
        final exists = await _checkIfSavingsAccountExists(user.id);
        if (exists) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Profile()));
        }
      }
    });
  }

  fetchUser(accessToken) async {
    try {
      return await Provider.of<SavingsAccount>(context, listen: false)
          .createOrAuth(accessToken);
    } catch (e) {
      print(e);
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _handleSignIn() async {
    final signIn = await googleSignIn.signIn();
    final auth = await signIn.authentication;
    final User user = await fetchUser(auth.accessToken);
    print(user.email);
    final exists = await _checkIfSavingsAccountExists(user.id);
    print(exists);
    if (exists) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => Profile()));
    } else {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => SetSavings()));
    }
  }

  Future<void> handleSignOut() => googleSignIn.disconnect();

  @override
  Widget build(BuildContext context) {
    return SignInButton(
      Buttons.Google,
      onPressed: () {
        _handleSignIn();
      },
    );
    // if (Platform.isIOS) {
    //   return SignInWithAppleButton(
    //     onPressed: () async {
    //       final credential = await SignInWithApple.getAppleIDCredential(
    //         webAuthenticationOptions: WebAuthenticationOptions(
    //             clientId: env['APPLE_CLIENT_ID'],
    //             redirectUri: Uri.parse(
    //               'https://www.scopuli.studio/accounts/apple/login/callback/',
    //             )),
    //         scopes: [
    //           AppleIDAuthorizationScopes.email,
    //           AppleIDAuthorizationScopes.fullName,
    //         ],
    //       );
    //       print(credential);
    //       // Now send the credential (especially `credential.authorizationCode`) to your server to create a session
    //       // after they have been validated with Apple (see `Integration` section for more information on how to do this)
    //     },
    //   );
    // } else if (Platform.isAndroid) {
    //   return SignInButton(
    //     Buttons.Google,
    //     onPressed: () {
    //       _handleSignIn();
    //     },
    //   );
      // return Container(
      //     width: double.infinity,
      //     child: Column(children: [
      //       SignInButton(
      //         Buttons.Google,
      //         onPressed: () {
      //           _handleSignIn();
      //         },
      //       ),
      //       SignInButton(
      //         Buttons.Facebook,
      //         onPressed: () {
      //           _handleSignIn();
      //         },
      //       )
      //     ]));
    }
  }
