import 'package:flutter/material.dart';

class BaseContainer extends StatelessWidget {
  final nextWidget;

  BaseContainer(this.nextWidget);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 24, right: 24, top: 30, bottom: 25),
      child: nextWidget,
    );
  }
}
