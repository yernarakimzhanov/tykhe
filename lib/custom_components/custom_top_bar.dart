import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'sign_in.dart';

class CustomTopBar extends StatelessWidget implements PreferredSizeWidget {
  final String customText;
  final bool removeLeftIcon;
  final bool removeRightIcon;
  final bool disconnectAuth;

  Size get preferredSize => const Size.fromHeight(60);

  CustomTopBar(this.customText,
      {this.removeLeftIcon = false,
      this.removeRightIcon = true,
      this.disconnectAuth = false});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      primary: true,
      backgroundColor: Colors.white,
      elevation: 0,
      leading: TextButton(
          onPressed: () {
            Navigator.of(context).pop();
            if (disconnectAuth) {
              googleSignIn.disconnect();
            }
          },
          child: SvgPicture.asset(
            'assets/svg/arrow-back.svg',
            width: 25,
            height: 25,
          )),
      bottomOpacity: 1,
      title: Text("$customText",
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
      actions: [
        if (!removeRightIcon)
          TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: SvgPicture.asset(
                'assets/svg/cross-exit.svg',
                width: 25,
                height: 25,
              ))
      ],
    );
  }
}
