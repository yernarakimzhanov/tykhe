import 'package:flutter/material.dart';

class ModalBaseContainer extends StatelessWidget {
  final nextWidget;

  ModalBaseContainer(this.nextWidget);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 15, right: 10, bottom: 20 ),
      child: nextWidget,
    );
  }
}
