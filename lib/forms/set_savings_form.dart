import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:tykhe/screens/profile.dart';
import 'package:tykhe/state/SavingsAccount.dart';

class SetSavingsForm extends StatefulWidget {
  @override
  SetSavingsFormState createState() => SetSavingsFormState();
}

class SetSavingsFormState extends State<SetSavingsForm> {
  final _formKey = GlobalKey<FormState>();
  final savingsAmountController = TextEditingController();
  final apyController = TextEditingController();

  String _selectedRate = 'weekly';
  double _age = 22;
  double _stopSavingAge = 80;
  Color _customGray = const Color(0xFFF4F4F4);

  final snackBar =
      SnackBar(content: Text('Sorry! Couldn`t connect to server!'));
  List<Map> _savingsPeriod = [
    {
      'name': 'weekly',
      'letter': 'Weekly',
      'isSelected': true,
    },
    {
      'name': 'biweekly',
      'letter': 'Biweekly',
      'isSelected': false,
    },
    {
      'name': 'monthly',
      'letter': 'Monthly',
      'isSelected': false,
    }
  ];

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    savingsAmountController.dispose();
    apyController.dispose();
    super.dispose();
  }

  List<Widget> _buildButtons() {
    List<Widget> listButtons =
        List<Widget>.generate(_savingsPeriod.length, (i) {
      return ButtonTheme(
        minWidth: 45,
        child: RaisedButton(
          color: _savingsPeriod[i]['isSelected'] ? Colors.black : _customGray,
          onPressed: () {
            setState(() {
              //TODO: Refactor this code smell
              for (var element in _savingsPeriod) {
                element['isSelected'] = false;
              }
              _savingsPeriod[i]['isSelected'] = true;
              _selectedRate = _savingsPeriod[i]['name'];
            });
          },
          child: Text("${_savingsPeriod[i]['letter']}",
              style: _savingsPeriod[i]['isSelected']
                  ? TextStyle(color: Colors.white)
                  : TextStyle(color: Colors.black)),
        ),
      );
    });
    return listButtons;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
            child: Form(
          key: _formKey,
          child: FractionallySizedBox(
            heightFactor: 0.75,
            widthFactor: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Savings rate',
                      style: TextStyle(fontSize: 17),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: _buildButtons(),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${toBeginningOfSentenceCase(_selectedRate)} contributions',
                      style: TextStyle(fontSize: 17),
                    ),
                    TextFormField(
                      controller: savingsAmountController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter your amount of monthly contributions";
                        }
                        return null;
                      },
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: false),
                      decoration: InputDecoration(
                        hintStyle: TextStyle(color: Colors.grey),
                        hintText: '1000',
                        // I used prefixIcon because prefix doesnt show when focus is not on current button
                        prefixIcon: Container(
                          padding: EdgeInsets.only(
                            right: 10.0,
                          ),
                          child: SvgPicture.asset('assets/svg/dollarSign.svg'),
                        ),
                        prefixIconConstraints:
                            BoxConstraints(minWidth: 0, minHeight: 0),
                        enabled: true,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Annual interest rate',
                      style: TextStyle(fontSize: 17),
                    ),
                    TextFormField(
                      controller: apyController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter your annual interest rate";
                        }
                        return null;
                      },
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                            RegExp(r'^\d+\.?\d{0,2}')),
                        LengthLimitingTextInputFormatter(3)
                      ],
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintStyle: TextStyle(color: Colors.grey),
                        hintText: '1.75',
                        suffixIconConstraints:
                            BoxConstraints(minWidth: 0, minHeight: 0),
                        prefixIcon: Container(
                          padding: EdgeInsets.only(
                            right: 10.0,
                          ),
                          child: SvgPicture.asset(
                            'assets/svg/percent.svg',
                          ),
                        ),
                        prefixIconConstraints:
                            BoxConstraints(minWidth: 0, minHeight: 0),
                        enabled: true,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Your age", style: TextStyle(fontSize: 17)),
                        Text(
                          _age.toStringAsFixed(0),
                        )
                      ],
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: CupertinoSlider(
                        value: _age,
                        min: 18,
                        max: 100,
                        divisions: 100,
                        activeColor: Colors.black,
                        onChanged: (double value) {
                          setState(() {
                            _age = value;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("When do you intend to stop saving?",
                            style: TextStyle(fontSize: 17)),
                        Text(
                          _stopSavingAge.toStringAsFixed(0),
                        )
                      ],
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: CupertinoSlider(
                        value: _stopSavingAge,
                        min: 18,
                        max: 100,
                        divisions: 100,
                        activeColor: Colors.black,
                        onChanged: (double value) {
                          setState(() {
                            _stopSavingAge = value;
                          });
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        )),
        SizedBox(
          height: 56,
          width: double.infinity,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(0)),
            color: Colors.black,
            child: Text(
              "start".toUpperCase(),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2.5),
            ),
            onPressed: () async {
              if (_formKey.currentState.validate()) {
                try {
                  await Provider.of<SavingsAccount>(context, listen: false)
                      .createSavingsAccount(
                          savingsPeriod: _selectedRate,
                          savingsAmount:
                              int.parse(savingsAmountController.text),
                          apy: double.parse(apyController.text),
                          age: _age.toInt(),
                          savingsStopAge: _stopSavingAge.toInt());
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()));
                } catch (e) {
                  Scaffold.of(context).showSnackBar(snackBar);
                }
              }
            },
          ),
        )
      ],
    );
  }
}
