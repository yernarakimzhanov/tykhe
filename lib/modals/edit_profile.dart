// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
//
// class EditProfile extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         margin: const EdgeInsets.only(left: 19, right: 24, top: 50, bottom: 20),
//         child: Column(
//           children: [
//             Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   GestureDetector(
//                     child: SvgPicture.asset('assets/svg/arrow-back.svg'),
//                     onTap: () => Navigator.of(context).pop(),
//                   ),
//                   Text("Edit profile",
//                       style: TextStyle(
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 17)),
//                   GestureDetector(
//                       onTap: () =>
//                           {Navigator.pop(context), Navigator.pop(context)},
//                       child: SvgPicture.asset("assets/svg/cross-exit.svg"))
//                 ]),
//             SizedBox(
//               height: 50,
//             ),
//             ProfileForm(),
//             Spacer(),
//             SizedBox(
//               height: 56,
//               width: double.infinity,
//               child: OutlinedButton(
//                 style: OutlinedButton.styleFrom(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: new BorderRadius.circular(0)),
//                   side: BorderSide(width: 2, color: Colors.black),
//                 ),
//                 child: Text(
//                   "save changes".toUpperCase(),
//                   style: TextStyle(
//                       color: Colors.black,
//                       fontSize: 16,
//                       fontWeight: FontWeight.w600,
//                       letterSpacing: 2.5),
//                 ),
//                 onPressed: () {},
//               ),
//             )
//           ],
//         ));
//   }
// }
