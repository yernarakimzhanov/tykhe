import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:tykhe/custom_components/modal_base_container.dart';
import 'package:tykhe/modals/edit_savings.dart';

import 'edit_profile.dart';

class ProfileOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ModalBaseContainer(Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListView(
          shrinkWrap: true,
          children: <Widget>[
            ListTile(
                title: Center(
                    child: Text('Settings',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17))),
                trailing: GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: SvgPicture.asset('assets/svg/cross-exit.svg'),
                )),
            GestureDetector(
              onTap: () => {
                showMaterialModalBottomSheet(
                    expand: true,
                    isDismissible: true,
                    context: context,
                    builder: (context) => EditSavings())
              },
              child: ListTile(
                title: Text('Savings'),
                subtitle: Text('Edit your savings preferences'),
                trailing: SvgPicture.asset('assets/svg/arrow-front.svg'),
              ),
            ),
            //TODO: Add dark mode
            // SwitchListTile(
            //   value: true,
            //   title: const Text('Dark mode'),
            //   onChanged: null,
            // ),
            Divider(),
            GestureDetector(
              onTap: () => {Navigator.of(context).pop()},
              child: ListTile(
                title: Text('Terms of service'),
                trailing: SvgPicture.asset('assets/svg/arrow-front.svg'),
              ),
            ),
            GestureDetector(
              onTap: () => {Navigator.of(context).pop()},
              child: ListTile(
                title: Text('Contact us'),
                trailing: SvgPicture.asset('assets/svg/arrow-front.svg'),
              ),
            ),
          ],
        ),
        Spacer(),
        SizedBox(
          height: 56,
          width: double.infinity,
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(0)),
              side: BorderSide(width: 2, color: Colors.black),
            ),
            child: Text(
              "sign out".toUpperCase(),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2.5),
            ),
            onPressed: () {},
          ),
        )
      ],
    ));
  }
}
