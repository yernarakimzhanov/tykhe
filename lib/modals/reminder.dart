import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:tykhe/custom_components/modal_base_container.dart';
import 'package:tykhe/services/notifications.dart';
import 'package:tykhe/state/SavingsAccount.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Reminder extends StatefulWidget {
  @override
  _ReminderState createState() => _ReminderState();
}

class _ReminderState extends State<Reminder> {
  DateTime _time = DateTime.now();
  Color _customGray = const Color(0xFFF4F4F4);

  List<Map> _daysOfWeek = [
    {
      'name': 'Sunday',
      'letter': 'T',
      'isSelected': false,
    },
    {
      'name': 'Monday',
      'letter': 'M',
      'isSelected': false,
    },
    {
      'name': 'Tuesday',
      'letter': 'T',
      'isSelected': false,
    },
    {
      'name': 'Wednesday',
      'letter': 'W',
      'isSelected': false,
    },
    {
      'name': 'Thursday',
      'letter': 'T',
      'isSelected': false,
    },
    {
      'name': 'Friday',
      'letter': 'F',
      'isSelected': false,
    },
    {
      'name': 'Saturday',
      'letter': 'S',
      'isSelected': false,
    },
  ];

  List<Widget> _buildButtons() {
    List<Widget> listButtons = List.generate(_daysOfWeek.length, (i) {
      return ButtonTheme(
        minWidth: 35,
        child: RaisedButton(
          color: _daysOfWeek[i]['isSelected'] ? Colors.black : _customGray,
          onPressed: () {
            setState(() {
              _daysOfWeek[i]['isSelected'] = !_daysOfWeek[i]['isSelected'];
            });
          },
          child: Text("${_daysOfWeek[i]['letter']}",
              style: _daysOfWeek[i]['isSelected']
                  ? TextStyle(color: Colors.white)
                  : TextStyle(color: Colors.black)),
        ),
      );
    });
    return listButtons;
  }

  @override
  Widget build(BuildContext context) {
    return ModalBaseContainer(
        Consumer<SavingsAccount>(builder: (context, savingsaccount, child) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListView(
            shrinkWrap: true,
            children: <Widget>[
              ListTile(
                  title: Center(
                    child: Text('Reminder',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17)),
                  ),
                  trailing: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: SvgPicture.asset('assets/svg/cross-exit.svg'),
                  )),
              Container(
                height: MediaQuery.of(context).size.height * 0.4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        'Savings rate: ${toBeginningOfSentenceCase(savingsaccount.savingsAccount.savingsPeriod)}',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 0.2)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Repeat:',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.2)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: _buildButtons(),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              backgroundColor: _customGray,
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(0)),
                              side: BorderSide(width: 2, color: Colors.white),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Choose time",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      DateFormat.jm().format(_time),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SvgPicture.asset(
                                        'assets/svg/arrow-front.svg')
                                  ],
                                )
                              ],
                            ),
                            onPressed: () {
                              {
                                showMaterialModalBottomSheet(
                                  isDismissible: true,
                                  context: context,
                                  builder: (context) => SingleChildScrollView(
                                    child: Container(
                                      height: 200,
                                      width: 200,
                                      child: CupertinoDatePicker(
                                        mode: CupertinoDatePickerMode.time,
                                        onDateTimeChanged: (dateTime) {
                                          setState(() {
                                            _time = dateTime;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(0)),
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              color: _customGray,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Savings reminder",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          )),
                                      Text(
                                          '${DateFormat.jm().format(savingsaccount.reminderTime)}',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 28,
                                          )),
                                      Divider(
                                        color: Colors.black,
                                        height: 20,
                                        thickness: 5,
                                        indent: 20,
                                        endIndent: 0,
                                      ),
                                      Text(
                                          //TODO:Fix this
                                          "Frequency: ${savingsaccount.savingsAccount.savingsPeriod}",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12,
                                          )),
                                    ],
                                  ),
                                  Spacer(),
                                  Container(
                                    height: 100,
                                    width: 100,
                                    child: ListTile(
                                      title: Text('Lights'),
                                      trailing: CupertinoSwitch(
                                        activeColor: Colors.black,
                                        value: savingsaccount.reminderEnabled,
                                        onChanged: (val) {
                                          savingsaccount
                                              .disableOrEnableReminder(val);
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
          Spacer(),
          SizedBox(
            height: 56,
            width: double.infinity,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(0)),
                side: BorderSide(width: 2, color: Colors.black),
              ),
              child: Text(
                "save changes".toUpperCase(),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 2.5),
              ),
              onPressed: () {
                savingsaccount.updateReminderTime(_time);
                updateTime(_time);
              },
            ),
          )
        ],
      );
    }));
  }
}

void updateTime(value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  initializeLocalNotifications();
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(value);

  prefs.setString('date', formatted);
}
