import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:tykhe/state/SavingsAccount.dart';

class AdditionalContributionModal extends StatefulWidget {
  @override
  _AdditionalContributionModal createState() => _AdditionalContributionModal();
}

class _AdditionalContributionModal extends State<AdditionalContributionModal> {
  final savingsAmountController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    savingsAmountController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Additional contribution'),
              OutlineButton(
                  focusColor: Colors.red,
                  borderSide: BorderSide(color: Colors.black),
                  color: Colors.black,
                  child: Text(
                    'ADD',
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () async {
                    try {
                      await Provider.of<SavingsAccount>(context, listen: false)
                          .addSavingsAmount(
                              int.parse(savingsAmountController.text));
                      Navigator.of(context).pop();
                    } catch (e) {
                      print(e);
                      // Scaffold.of(context).showSnackBar(snackBar);
                    }
                  }),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: TextField(
              controller: savingsAmountController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  isDense: true,
                  icon: SvgPicture.asset('assets/svg/dollarSign.svg'),
                  hintText: '0.0'),
              autofocus: true,
            ),
          ),
        ],
      ),
    );
  }
}
