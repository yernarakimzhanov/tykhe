import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tykhe/custom_components/base_container.dart';
import 'package:tykhe/custom_components/modal_base_container.dart';
import 'package:tykhe/forms/set_savings_form.dart';

class EditSavings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ModalBaseContainer(Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListView(
          shrinkWrap: true,
          children: <Widget>[
            ListTile(
                title: Center(
                    child: Text('Edit savings',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17))),
                trailing: GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: SvgPicture.asset('assets/svg/cross-exit.svg'),
                )),
          ],
        ),
        Expanded(child: SetSavingsForm()),
        Spacer(),
        // SizedBox(
        //   height: 56,
        //   width: double.infinity,
        //   child: OutlinedButton(
        //     style: OutlinedButton.styleFrom(
        //       shape: RoundedRectangleBorder(
        //           borderRadius: new BorderRadius.circular(0)),
        //       side: BorderSide(width: 2, color: Colors.black),
        //     ),
        //     child: Text(
        //       "save changes".toUpperCase(),
        //       style: TextStyle(
        //           color: Colors.black,
        //           fontSize: 16,
        //           fontWeight: FontWeight.w600,
        //           letterSpacing: 2.5),
        //     ),
        //     onPressed: () {},
        //   ),
        // )
      ],
    ));
  }
}
