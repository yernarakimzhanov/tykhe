import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:tykhe/screens/registration.dart';
import 'package:tykhe/screens/set_savings.dart';
import 'package:tykhe/screens/profile.dart';
import 'package:tykhe/state/SavingsAccount.dart';

void main() async {
  await DotEnv.load(fileName: ".env");
  runApp(ChangeNotifierProvider(
    create: (context) => SavingsAccount(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            currentFocus.focusedChild.unfocus();
          }
        },
        child: MaterialApp(
          theme: ThemeData(
            bottomSheetTheme:
                BottomSheetThemeData(backgroundColor: Colors.white),
            scaffoldBackgroundColor: Colors.white,
            fontFamily: 'Gilroy',
            primaryColor: Colors.black,
            accentColor: Colors.black,
          ),
          routes: {
            '/': (context) => Registration(),
            '/setsavings': (context) => SetSavings(),
            '/profile': (context) => Profile(),
          },
          color: Colors.black,
        ));
  }
}
