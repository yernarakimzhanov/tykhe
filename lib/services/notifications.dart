import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future<void> initializeLocalNotifications() async {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('icon');
  final IOSInitializationSettings initializationSettingsIOS =
  IOSInitializationSettings(
      onDidReceiveLocalNotification: onDidReceiveLocalNotification);
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: selectNotification);
  const AndroidNotificationDetails androidPlatformChannelSpecifics =
  AndroidNotificationDetails('repeating channel id',
      'repeating channel name', 'repeating description');
  const NotificationDetails platformChannelSpecifics =
  NotificationDetails(android: androidPlatformChannelSpecifics);
  //TODO: Code below starts notifications
  // flutterLocalNotificationsPlugin.periodicallyShow(0, 'Time to start tracking your savings ',
  //     '', RepeatInterval.everyMinute, platformChannelSpecifics,
  //     androidAllowWhileIdle: true);
}

Future selectNotification(String payload) {
//  TODO: Implement what to do on notification click
}

Future onDidReceiveLocalNotification(
    //  TODO: Implement what to do on notification click
    int id,
    String title,
    String body,
    String payload) {}
