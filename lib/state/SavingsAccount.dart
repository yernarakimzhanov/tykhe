import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class SavingsHistory {
  final int id;
  final String date;
  final int amount;
  final String status;

  SavingsHistory({this.id, this.date, this.amount, this.status});

  factory SavingsHistory.fromJson(Map<String, dynamic> json) {
    return SavingsHistory(
      id: json['id'],
      date: json['date'],
      amount: json['amount'],
      status: json['status'],
    );
  }
}

class User {
  final int id;
  final String username;
  final String email;
  final String firstName;
  final String lastName;
  final String accessToken;
  final String refreshToken;

  User(
      {this.accessToken,
      this.refreshToken,
      this.id,
      this.username,
      this.email,
      this.firstName,
      this.lastName});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        accessToken: json['access_token'],
        refreshToken: json['refresh_token'],
        id: json['user']['pk'],
        username: json['user']['username'],
        email: json['user']['email'],
        firstName: json['user']['first_name'],
        lastName: json['user']['last_name']);
  }
}

class SavingsAccountModel {
  final int id;
  final String savingsPeriod;
  final int savingsGoal;
  final int savingsAmount;
  final String apy;
  final int savingsStopAge;
  final int currentSavings;

  SavingsAccountModel(
      {this.id,
      this.savingsPeriod,
      this.savingsGoal,
      this.savingsAmount,
      this.apy,
      this.savingsStopAge,
      this.currentSavings});

  factory SavingsAccountModel.fromJson(Map<String, dynamic> json) {
    return SavingsAccountModel(
        id: json['id'],
        savingsPeriod: json['savings_period'],
        savingsAmount: json['savings_amount'],
        savingsGoal: json['savings_goal'],
        apy: json['apy'],
        savingsStopAge: json['savings_stop_age'],
        currentSavings: json['current_savings']);
  }
}

class SavingsAccount extends ChangeNotifier {
  final String _backendUrl = env['BACKEND'];
  final String _backendUrlWithoutPrefix = env['BACKEND_URL_WITHOUT_PREFIX'];
  bool _isLoggedIn = false;
  Map _reminderTime = {
    "time": DateTime.now(),
    "enabled": false,
  };
  var user;
  var _savingsAccount;
  var _savingsHistoryList = [];

  get savingsAccount => _savingsAccount;

  get reminderTime => _reminderTime['time'];

  get reminderEnabled => _reminderTime['enabled'];

  void updateReminderTime(DateTime newTime) {
    _reminderTime['time'] = newTime;
    notifyListeners();
  }

  void disableOrEnableReminder(val) {
    _reminderTime['enabled'] = val;
    notifyListeners();
  }

  Future<User> createOrAuth(String accessToken) async {
    final response = await http
        .post('$_backendUrl/google/login', body: {'access_token': accessToken});
    if (response.statusCode == 200) {
      user = User.fromJson(jsonDecode(response.body));
      notifyListeners();
      return user;
    } else {
      throw Exception("Couldn`t connect to server");
    }
  }

  Future<SavingsAccountModel> fetchSavingsAccount(String userId) async {
    var queryParameters = {"user_id": userId};
    var uri = Uri.http(
      '$_backendUrlWithoutPrefix',
      '/api/savingsaccount',
      queryParameters,
    );
    var response = await http.get(uri);
    if (response.statusCode == 200) {
      _savingsAccount = SavingsAccountModel.fromJson(jsonDecode(response.body));
      notifyListeners();
      return _savingsAccount;
    } else if (response.statusCode == 404) {
      return null;
    } else {
      throw Exception("Couldn`t connect to server");
    }
  }

  Future<SavingsAccountModel> addSavingsAmount(int savingsAmount) async {
    try {
      final response = await http.patch(
          '$_backendUrl/api/savingsaccount/${savingsAccount.id}/add-savings/',
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode({'amount': savingsAmount}));
      if (response.statusCode == 200) {
        _savingsAccount =
            SavingsAccountModel.fromJson(jsonDecode(response.body));
        notifyListeners();
        return _savingsAccount;
      }
    } catch (e) {
      print(e);
    }
  }

  Future<SavingsAccountModel> createSavingsAccount(
      {@required String savingsPeriod,
      @required int savingsAmount,
      @required double apy,
      @required int age,
      @required int savingsStopAge}) async {
    try {
      final response =
          await http.post('$_backendUrl/api/savingsaccount/create/',
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode({
                'user': user.id,
                "savings_period": savingsPeriod,
                "savings_amount": savingsAmount,
                "apy": apy,
                "age": age,
                "savings_stop_age": savingsStopAge
              }));
      if (response.statusCode == 201) {
        _savingsAccount =
            SavingsAccountModel.fromJson(jsonDecode(response.body));
        notifyListeners();
        return _savingsAccount;
      } else {
        throw ("Server error");
      }
    } catch (e) {
      print(e);
      throw Exception("Couldn`t connect to server");
    }
  }

  Future fetchSavingsHistory() async {
    final response = await http.get(
        '$_backendUrl/api/savingsaccount/${_savingsAccount.id}/savingshistory/');
    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      if (data.isNotEmpty) {
        for (var i in data) {
          _savingsHistoryList.add(SavingsHistory.fromJson(i));
        }
      }
      notifyListeners();
      return _savingsHistoryList;
    } else {
      throw Exception("Couldn`t connect to server");
    }
  }
}
